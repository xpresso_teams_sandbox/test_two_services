"""
This is a sample hello world flask app
It only has a root resource which sends back hello World html text
"""
__author__ = "Naveen Sinha"


import logging
import os

import requests as req
from flask import Flask, render_template
from flask import request
from xpresso.ai.core.logging.xpr_log import XprLogger

config_file = 'config/dev.json'

logger = XprLogger("ui_service", level=logging.INFO)


def create_app() -> Flask:
    """
    Method to initialize the flask app. It should contain all the flask
    configuration

    Returns:
         Flask: instance of Flask application
    """
    flask_app = Flask(__name__)
    return flask_app


app = create_app()


@app.route('/')
def hello_world():
    """
    Send response to Hello World
    """

    logger.info("Received request from {}".format(request.remote_addr))
    return render_template('form.html')


@app.route('/result', methods=['POST'])
def form_ui():
    """
    Basic form to test two services using a service mesh
    """

    logger.info('Receiving request for the HTML page')
    result = request.form.to_dict()

    dnn_url = "http://test-two-services--inf-service-1.test-two-services:5000/predict"
    xgboost_url = "http://test-two-services--inf-service-2.test-two-services:5000/predict"
    if result['DNN'] == '' and result['XGBoost'] == '':
        return '<html><body><b>Invalid form request</b></body></html>'
    if result['XGBoost'] == '':
        response = req.post(url=dnn_url, json={'input': result["DNN"]})
        response = response.json()
    elif result['DNN'] == '':
        response = req.post(url=xgboost_url, json={'input': result["XGBoost"]})
        response = response.json()
    else:
        response1 = req.post(url=dnn_url, json={'input': result["DNN"]})
        response2 = req.post(url=xgboost_url, json={'input': result["XGBoost"]})
        response = {
            'results': response1.json()['results'] + ' ' + response2.json()['results']
        }
    return '<html><body><b>Result: {}</b></body></html>'.format(response['results'])


if __name__ == '__main__':
    app.run(host="localhost", port=5000)
